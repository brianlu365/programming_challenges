I did line_of_credit problem in rails and factors_and_caching problem in ruby.

for line_of_credit: 

I made a seed file for you to start with. run rake db:reset. 
There is only one page. You can add/remove transaction and see a list of transaction. On the last line of the list it shows the summarized balance on the closing the next 30 days period
So if you entered a new transaction for day 31, it will show balance for day 60.

for factors_and_caching:

I used a gem called [shuhari](https://github.com/jarhart/shuhari) to generate a kata enviroment. run bundle first then run rspec to run all tests.
# answer to questions
# 1.  

I implemented a cache class that stores key value pair. It uses the hash of the sorted array input as key and the result as the value.

# 2.

Hash lookup is O(1) and sorting the array is probably nlog(n) plus hashing the array.

I benchmarked hash lookup with integer key and array key. Integer key is faster. 
Of course a better hash method might be needed since "Any hash value that exceeds the capacity of a Fixnum will be truncated before being used."-from rubydoc.

To make it more performant, add caching at multiple levels. Maybe a subset of the array is cached. so instead if [10, 5, 2, 20] missed in the cache, we can calculate 10 against [5,2,20].
then when we moving on to calculate 5 against [2,20] we can check the cache again. 

# 3.

No. I don't think it will change the caching algorithim since it is a key value pair caching.




We ask that you do at least 1 in Ruby and 1 in a language  of your choice.

For mobile engineering candidates (iOS or Android), please complete the [mobile challenge](https://github.com/avantcredit/programming_challenges/blob/master/mobile.md) only. 

Please do not fork this repository for your answers; that would allow other candidates to see your code. We'll accept most formats, but here are three that others have found useful:
- Duplicate the repository *without forking*. It can still be public. [Github instructions here](https://help.github.com/articles/duplicating-a-repository/)
- One or more [Github Gists](https://gist.github.com/)
- Email a zip file containing your code
